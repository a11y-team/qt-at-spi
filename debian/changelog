REMOVED from unstable

qt-at-spi (0.4.0-10) UNRELEASED; urgency=medium

  * control: Set Vcs-* to salsa.debian.org.
  * control: Bump Standards-Version to 4.4.0 (no changes).

 -- Samuel Thibault <sthibault@debian.org>  Mon, 05 Aug 2019 21:29:52 +0200

qt-at-spi (0.4.0-9) unstable; urgency=medium

  * Bump Standards-Version to 4.2.0 (no changes).
  * control: Mark qt-at-spi-doc Multi-Arch: foreign.
  * patches/timeout: Increase at-spi timeout to 25s rather than just 1s, which
    could happen if the machine is just a bit sluggish. 25s is the normal dbus
    timeout.

 -- Samuel Thibault <sthibault@debian.org>  Wed, 24 Oct 2018 15:27:11 +0200

qt-at-spi (0.4.0-8) unstable; urgency=high

  * patches/nokde: Restore to previous version (Closes: Bug#887431)
  * Restore kdelibs5-dev build-dep.

 -- Samuel Thibault <sthibault@debian.org>  Tue, 16 Jan 2018 17:57:10 +0100

qt-at-spi (0.4.0-7) unstable; urgency=medium

  [ Alexander Volkov ]
  * patches/nokde: Complete to completely avoid kde.
  * control: Remove kdelibs5-dev build-dep accordingly.

 -- Samuel Thibault <sthibault@debian.org>  Fri, 05 Jan 2018 19:38:54 +0100

qt-at-spi (0.4.0-6) unstable; urgency=medium

  [ Samuel Thibault ]
  * Use canonical anonscm vcs URL.
  * control: Update maintainer mailing list.
  * control: Bump Standards-Version to 4.1.1 (no change)

  [ Helmut Grohne ]
  * Indirect cmake through dh_auto_configure (Closes: #875748)

 -- Samuel Thibault <sthibault@debian.org>  Sun, 22 Oct 2017 17:53:57 +0200

qt-at-spi (0.4.0-5) unstable; urgency=medium

  * control: Drop hardening-wrapper dependency (Closes: #836650)
  * rules: Set hardening, but not bindnow.

 -- Samuel Thibault <sthibault@debian.org>  Sun, 04 Sep 2016 18:53:18 +0200

qt-at-spi (0.4.0-4) unstable; urgency=medium

  * control: Bump Standards-Version to 3.9.8 (no change).
  * control: Build-Depend on dbus instead of dbus-x11, and run testsuite through
    dbus-run-session.
  * rules: Drop simple and tabbar binaries from tarball.

 -- Samuel Thibault <sthibault@debian.org>  Mon, 29 Aug 2016 22:25:38 +0200

qt-at-spi (0.4.0-3) unstable; urgency=medium

  * Add watch file.
  * patches/nokde: Comment out translation support to avoid libkdecore5
    dependency.  We don't have any translation and probably won't, anyway.
  * rules: Remove generated documentation in clean rule.

 -- Samuel Thibault <sthibault@debian.org>  Sat, 14 Nov 2015 19:17:40 +0100

qt-at-spi (0.4.0-2) unstable; urgency=medium

  * rules: Unless DEB_BUILD_OPTIONS contains debug, define
    QT_NO_DEBUG_STREAM QT_NO_WARNING_OUTPUT to disable debugging output.
  * patches/quiet: Remove patch.
  * patches/noassert: Complete patch to replace assertions with warnings.

 -- Samuel Thibault <sthibault@debian.org>  Wed, 14 Oct 2015 23:25:45 +0200

qt-at-spi (0.4.0-1) unstable; urgency=medium

  * New upstream release (Closes: #801057).
    - patches/werror: Remove, not needed any more.
    - patches/getcharacterextents-marshalling: Refresh.
    - patches/noassert: Refresh.
    - control: Add cmake, kdelibs5-dev, xvfb, dbus-x11, and at-spi2-core
      build-depends.
    - rules: Fix multi-arch plugin path, run dh_auto_test inside xvfb-run.

 -- Samuel Thibault <sthibault@debian.org>  Thu, 08 Oct 2015 03:23:15 +0200

qt-at-spi (0.3.1-8) unstable; urgency=medium

  * patches/noassert: Avoid using assertions, which would crash applications...

 -- Samuel Thibault <sthibault@debian.org>  Fri, 25 Sep 2015 01:45:00 +0200

qt-at-spi (0.3.1-7) unstable; urgency=medium

  * patches/quiet: New patch to shut successful initialization messages down.

 -- Samuel Thibault <sthibault@debian.org>  Fri, 28 Aug 2015 00:26:53 +0200

qt-at-spi (0.3.1-6) unstable; urgency=medium

  * patches/getcharacterextents-marshalling: New patch from QT5 to
    fix marshalling of GetCharacterExtents and GetRangeExtents
    (Closes: #792208).

 -- Samuel Thibault <sthibault@debian.org>  Wed, 12 Aug 2015 00:40:14 +0200

qt-at-spi (0.3.1-5) unstable; urgency=low

  * patches/timeout: Mitigate kde application startup time when the at-spi bus
    is hosed (Closes: #762672).
  * Bump Standards-Version to 3.9.6 (no changes).

 -- Samuel Thibault <sthibault@debian.org>  Thu, 29 Jan 2015 02:56:29 +0100

qt-at-spi (0.3.1-4) unstable; urgency=low

  * patches/werror: New patch to drop use of -Werror (Closes: #701343).

 -- Samuel Thibault <sthibault@debian.org>  Sun, 30 Jun 2013 23:54:58 +0200

qt-at-spi (0.3.1-3) unstable; urgency=low

  * Split out API documentation into new qt-at-spi-doc package. This should
    permit qt-at-spi to be really multi-arch-installable (Closes: #689403).

 -- Samuel Thibault <sthibault@debian.org>  Thu, 04 Oct 2012 02:04:59 +0200

qt-at-spi (0.3.1-2) unstable; urgency=low

  [ Luke Yelavich ]
  * Declare multi-arch support, so the bridge can be used for 32bit
    applications.

  [ Samuel Thibault ]
  * rules: Disable bindnow hardening, it completely breaks loading the whole
    bridge (Closes: #688988).

 -- Samuel Thibault <sthibault@debian.org>  Mon, 01 Oct 2012 01:41:13 +0200

qt-at-spi (0.3.1-1) unstable; urgency=low

  [ Luke Yelavich ]
  * Initial release (Closes: #629870)

 -- Samuel Thibault <sthibault@debian.org>  Fri, 08 Jun 2012 23:02:24 +0200
