commit f3e86a8cc87754d7efbd33c15f61d554ce3160f0
Author: Frederik Gladhorn <frederik.gladhorn@digia.com>
Date:   Tue Jun 17 19:10:43 2014 +0200

    Accessibility Linux: Fix methods returning rects as iiii for AT-SPI
    
    According to the spec rects get returned with iiii but we were directly
    serializing QRect resulting in (iiii) as signature.
    This would trip up Orca when trying to use flat review in text edits.
    
    Task-number: QTBUG-39702
    Change-Id: I8d6769688586e678d27cc4341de5176a91f057fc
    Reviewed-by: Gabriel de Dietrich <gabriel.dedietrich@digia.com>

--- a/src/atspiadaptor.cpp
+++ b/src/atspiadaptor.cpp
@@ -2100,14 +2100,14 @@ QList<QVariant> AtSpiAdaptor::getCharact
     return QList<QVariant>() << rect.x() << rect.y() << rect.width() << rect.height();
 }
 
-QRect AtSpiAdaptor::getRangeExtents(QAccessibleInterface *interface,
+QList<QVariant> AtSpiAdaptor::getRangeExtents(QAccessibleInterface *interface,
                                     int startOffset, int endOffset, uint coordType) const
 {
     if (endOffset == -1)
         endOffset = interface->textInterface()->characterCount();
 
     if (endOffset <= startOffset) {
-        return QRect();
+        return QList<QVariant>() << -1 << -1 << 0 << 0;
     }
 
     QRect rect = interface->textInterface()->characterRect(startOffset, QAccessible2::RelativeToScreen);
@@ -2119,7 +2119,7 @@ QRect AtSpiAdaptor::getRangeExtents(QAcc
     if (coordType == ATSPI_COORD_TYPE_WINDOW)
         rect = translateRectToWindowCoordinates(interface, rect);
 
-    return rect;
+    return QList<QVariant>() << rect.x() << rect.y() << rect.width() << rect.height();
 }
 
 QRect AtSpiAdaptor::translateRectToWindowCoordinates(QAccessibleInterface *interface, const QRect &rect)
--- a/src/atspiadaptor.h
+++ b/src/atspiadaptor.h
@@ -104,8 +104,8 @@ private:
     // text helper functions
     QVariantList getAttributes(QAccessibleInterface *interface, int offset, bool includeDefaults) const;
     QVariantList getAttributeValue(QAccessibleInterface *interface, int offset, const QString &attributeName) const;
-    QRect getCharacterExtents(QAccessibleInterface *interface, int offset, uint coordType) const;
-    QRect getRangeExtents(QAccessibleInterface *interface, int startOffset, int endOffset, uint coordType) const;
+    QList<QVariant> getCharacterExtents(QAccessibleInterface *interface, int offset, uint coordType) const;
+    QList<QVariant> getRangeExtents(QAccessibleInterface *interface, int startOffset, int endOffset, uint coordType) const;
     QAccessible2::BoundaryType qAccessibleBoundaryType(int atspiTextBoundaryType) const;
 
     static bool inheritsQAction(QObject *object);
