Avoid using assertions, which would crash applications...

--- a/src/atspiadaptor.cpp
+++ b/src/atspiadaptor.cpp
@@ -615,7 +615,11 @@ void AtSpiAdaptor::setInitialized(bool i
 
 void AtSpiAdaptor::setBitFlag(const QString &flag)
 {
-    Q_ASSERT(flag.size());
+    if (!(flag.size()))
+    {
+        qWarning() << "AtSpiAdaptor::setBitFlag: empty flag";
+        return;
+    }
 
     // assume we don't get nonsense - look at first letter only
     switch (flag.at(0).toLower().toLatin1()) {
@@ -976,12 +980,16 @@ void AtSpiAdaptor::notifyStateChange(QAc
 */
 void AtSpiAdaptor::notify(int reason, QAccessibleInterface *interface, int child)
 {
-    Q_ASSERT(interface);
+    if (!interface)
+    {
+        qWarning() << "AtSpiAdaptor::notify: no interface";
+        return;
+    }
 
     if (!interface->isValid()) {
         //spiBridge->removeAdaptor(this);
         // FIXME announce that this thing is dead? will it ever happen?
-        Q_ASSERT_X(0, "", "Got an update for an invalid inteface. Investigate this.");
+        qWarning("Got an update for an invalid inteface. Investigate this.");
         return;
     }
 
@@ -995,7 +1003,11 @@ void AtSpiAdaptor::notify(int reason, QA
     }
     // Saving of the last text should not be skipped, even when initialized is still false.
     if (reason == QAccessible::ObjectShow && interface->textInterface()) {
-        Q_ASSERT(interface->object());
+          if (!(interface->object()))
+          {
+            qWarning() << "AtSpiAdaptor::notify: no interface->object()";
+            return;
+          }
         QString text = interface->textInterface()->text(0, interface->textInterface()->characterCount());
         interface->object()->setProperty(ACCESSIBLE_LAST_TEXT, text);
     }
@@ -1050,7 +1062,10 @@ void AtSpiAdaptor::notify(int reason, QA
     }
     case QAccessible::TextUpdated: {
         if (sendObject || sendObject_text_changed) {
-            Q_ASSERT(interface->textInterface());
+            if (!interface || !interface->textInterface()) {
+                qDebug() << "Received text event for invalid interface.";
+                return;
+            }
             QString path = pathForInterface(interface, child);
             // at-spi doesn't have a text updated/changed, so remove all and re-add the new text
             QString oldText = interface->object()->property(ACCESSIBLE_LAST_TEXT).toString();
@@ -1099,7 +1114,10 @@ void AtSpiAdaptor::notify(int reason, QA
     }
     case QAccessible::ValueChanged: {
             if (sendObject || sendObject_value_changed) {
-                Q_ASSERT(interface->valueInterface());
+                if (!interface || !interface->valueInterface()) {
+                    qDebug() << "Received value event for invalid interface.";
+                    return;
+                }
                 QString path = pathForInterface(interface, child);
                 QVariantList args = packDBusSignalArguments(QLatin1String("accessible-value"), 0, 0, variantForPath(path));
                 sendDBusSignal(path, QLatin1String(ATSPI_DBUS_INTERFACE_EVENT_OBJECT),
@@ -1324,26 +1342,46 @@ bool AtSpiAdaptor::applicationInterface(
     }
 
     if (function == "SetId") {
-        Q_ASSERT(message.signature() == "ssv");
+        if (message.signature() != "ssv")
+        {
+          qWarning() << "AtSpiAdaptor::applicationInterface: SetId message signature is not 'ssv' but " << message.signature();
+          return false;
+        }
         QVariant value = qvariant_cast<QDBusVariant>(message.arguments().at(2)).variant();
 
         m_applicationId = value.toInt();
         return true;
 
     } else if (function == "GetId") {
-        Q_ASSERT(message.signature() == "ss");
+        if (message.signature() != "ss")
+        {
+          qWarning() << "AtSpiAdaptor::applicationInterface: GetId message signature is not 'ss' but " << message.signature();
+          return false;
+        }
         QDBusMessage reply = message.createReply(QVariant::fromValue(QDBusVariant(m_applicationId)));
         return connection.send(reply);
     } else if (function == "GetToolkitName") {
-        Q_ASSERT(message.signature() == "ss");
+        if (message.signature() != "ss")
+        {
+          qWarning() << "AtSpiAdaptor::applicationInterface: GetToolkitName message signature is not 'ss' but " << message.signature();
+          return false;
+        }
         QDBusMessage reply = message.createReply(QVariant::fromValue(QDBusVariant(QLatin1String("Qt"))));
         return connection.send(reply);
     } else if (function == "GetVersion") {
-        Q_ASSERT(message.signature() == "ss");
+        if (message.signature() != "ss")
+        {
+          qWarning() << "AtSpiAdaptor::applicationInterface: GetVersion message signature is not 'ss' but " << message.signature();
+          return false;
+        }
         QDBusMessage reply = message.createReply(QVariant::fromValue(QDBusVariant(QLatin1String(qVersion()))));
         return connection.send(reply);
     } else if (function == "GetLocale") {
-        Q_ASSERT(message.signature() == "u");
+        if (message.signature() != "u")
+        {
+          qWarning() << "AtSpiAdaptor::applicationInterface: GetLocale message signature is not 'u' but " << message.signature();
+          return false;
+        }
         QDBusMessage reply = message.createReply(QVariant::fromValue(QLocale().name()));
         return connection.send(reply);
     } else {
@@ -1586,7 +1624,11 @@ QAccessibleInterface *AtSpiAdaptor::acce
 
 QString AtSpiAdaptor::pathForObject(QObject *object) const
 {
-    Q_ASSERT(object);
+    if (!(object))
+    {
+        qWarning() << "AtSpiAdaptor::pathForObject: no object";
+        return ATSPI_DBUS_PATH_NULL;
+    }
 
     if (object->metaObject()->className() == QLatin1String("QAction")) {
         qDebug() << "AtSpiAdaptor::pathForObject: warning: creating path with QAction as object.";
@@ -1626,7 +1668,11 @@ QString AtSpiAdaptor::pathForInterface(Q
     while(!interfaceWithObject->object()) {
         QAccessibleInterface* parentInterface;
         interfaceWithObject->navigate(QAccessible::Ancestor, 1, &parentInterface);
-        Q_ASSERT(parentInterface->isValid());
+        if (!parentInterface->isValid())
+        {
+            qWarning() << "AtSpiAdaptor::pathForInterface: interface is not valid";
+            return ATSPI_DBUS_PATH_NULL;
+        }
         int index = parentInterface->indexOfChild(interfaceWithObject);
 
         if (index < 0) {
@@ -1885,7 +1931,7 @@ QSpiActionArray AtSpiAdaptor::getActions
 // Text interface
 bool AtSpiAdaptor::textInterface(QAccessibleInterface *interface, int child, const QString &function, const QDBusMessage &message, const QDBusConnection &connection)
 {
-    Q_ASSERT(child == 0); // We should never claim to have a text interface on a virtual child
+    //Q_ASSERT(child == 0); // We should never claim to have a text interface on a virtual child
     if (!interface->textInterface()) {
         qWarning() << "WARNING Qt AtSpiAdaptor: Could not find text interface for: " << message.path() << interface;
         return false;
@@ -1935,7 +1981,7 @@ bool AtSpiAdaptor::textInterface(QAccess
         int end;
         QString result = interface->textInterface()->textAtOffset(offset, QAccessible2::CharBoundary, &start, &end);
         sendReply(connection, message, (int) *(qPrintable (result)));
-        Q_ASSERT(0); // FIXME wtf is this!!!???!!! at least test if it produces the desired result bah
+        //Q_ASSERT(0); // FIXME wtf is this!!!???!!! at least test if it produces the desired result bah
 
     } else if (function == "GetCharacterExtents") {
         int offset = message.arguments().at(0).toInt();
@@ -2037,7 +2083,7 @@ QAccessible2::BoundaryType AtSpiAdaptor:
     case ATSPI_TEXT_BOUNDARY_LINE_END:
         return QAccessible2::LineBoundary;
     }
-    Q_ASSERT_X(0, "", "Requested invalid boundary type.");
+    qWarning() << "Requested invalid boundary type.";
     return QAccessible2::CharBoundary;
 }
 
@@ -2137,7 +2183,7 @@ QRect AtSpiAdaptor::translateRectToWindo
 // Editable Text interface
 bool AtSpiAdaptor::editableTextInterface(QAccessibleInterface *interface, int child, const QString &function, const QDBusMessage &message, const QDBusConnection &connection)
 {
-    Q_ASSERT(child == 0); // We should never claim to have a text interface on a virtual child
+    //Q_ASSERT(child == 0); // We should never claim to have a text interface on a virtual child
     if (!interface->editableTextInterface()) {
         qWarning() << "WARNING Qt AtSpiAdaptor: Could not find editable text interface for: " << message.path() << interface;
         return false;
@@ -2186,7 +2232,7 @@ bool AtSpiAdaptor::editableTextInterface
 // Value interface
 bool AtSpiAdaptor::valueInterface(QAccessibleInterface *interface, int child, const QString &function, const QDBusMessage &message, const QDBusConnection &connection)
 {
-    Q_ASSERT(child == 0);
+    //Q_ASSERT(child == 0);
     if (!interface->valueInterface()) {
         qWarning() << "WARNING Qt AtSpiAdaptor: Could not find value interface for: " << message.path() << interface;
         return false;
@@ -2237,7 +2283,7 @@ bool AtSpiAdaptor::valueInterface(QAcces
 // Table interface
 bool AtSpiAdaptor::tableInterface(QAccessibleInterface *interface, int child, const QString &function, const QDBusMessage &message, const QDBusConnection &connection)
 {
-    Q_ASSERT(child == 0);
+    //Q_ASSERT(child == 0);
     if (!interface->table2Interface()) {
         qWarning() << "WARNING Qt AtSpiAdaptor: Could not find table interface for: " << message.path() << interface;
         return false;
@@ -2317,7 +2363,7 @@ bool AtSpiAdaptor::tableInterface(QAcces
         QAccessibleInterface *cell = interface->table2Interface()->cellAt(row, column);
         int index = interface->indexOfChild(cell);
         qDebug() << "QSpiAdaptor::GetIndexAt" << row << column << index;
-        Q_ASSERT(index > 0);
+        //Q_ASSERT(index > 0);
         delete cell;
         connection.send(message.createReply(index));
 
